@extends('layouts.main')

@section('content')

    <!-- Page Title Starts -->
    <section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
        <h1>get in <span>touch</span></h1>
        <span class="title-bg">contact</span>
    </section>
    <!-- Page Title Ends -->

    <!-- Main Content Starts -->
    <section class="main-content revealator-slideup revealator-once revealator-delay1">
        <div class="container">
            <div class="row">
                <!-- Left Side Starts -->
                <div class="col-12 col-lg-4">
                    <h3 class="text-uppercase custom-title mb-0 ft-wt-600 pb-3">Don't be shy !</h3>
                    <p class="open-sans-font mb-3">Jangan ragu untuk menghubungi saya. Saya selalu terbuka untuk mendiskusikan proyek baru, ide kreatif atau peluang untuk menjadi bagian dari visi Anda.</p>
                    <p class="open-sans-font custom-span-contact position-relative">
                        <i class="fa fa-envelope-open position-absolute"></i>
                        <span class="d-block">mail me</span>adi.bawa@undiksha.ac.id
                    </p>
                    <p class="open-sans-font custom-span-contact position-relative">
                        <i class="fa fa-phone-square position-absolute"></i>
                        <span class="d-block">call me</span>+62 877 2345 4083
                    </p>
                    <ul class="social list-unstyled pt-1 mb-5">
                        <li class="facebook"><a title="Facebook" href="https://web.facebook.com/putu.adi.102"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li class="twitter"><a title="Twitter" href="https://www.instagram.com/adii_putu/"><i class="fa fa-instagram"></i></a>
                        </li>
                        <li class="youtube"><a title="Youtube" href="https://www.youtube.com/channel/UCdC7BHJEEjD9wccQo3uYKTg"><i class="fa fa-youtube"></i></a>
                        </li>
                        <li class="dribbble"><a title="WhatsApp" href="https://wa.wizard.id/36b281"><i class="fa fa-whatsapp"></i></a>
                        </li>
                    </ul>
                </div>
                <!-- Left Side Ends -->
                <!-- Contact Form Starts -->
                <div class="col-12 col-lg-8">
                    <form class="contactform" method="post" action="http://slimhamdi.net/tunis/dark/php/process-form.php">
                        <div class="contactform">
                            <div class="row">
                                <div class="col-12 col-md-4">
                                <img src="img/contact.jpg" alt="" width="650px" height="330px">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- Contact Form Ends -->
            </div>
        </div>

    </section>

@endsection